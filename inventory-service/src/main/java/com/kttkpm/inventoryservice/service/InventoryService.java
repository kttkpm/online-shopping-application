package com.kttkpm.inventoryservice.service;

import com.kttkpm.inventoryservice.dto.InventoryResponse;
import com.kttkpm.inventoryservice.repository.InventoryRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Slf4j
public class InventoryService {
  private final InventoryRepository inventoryRepository;

  @Transactional(readOnly = true)
  @SneakyThrows
  public List<InventoryResponse> isInStock(List<String> skuCode) {
    log.info("Wait started");
    Thread.sleep(10000);
    log.info("Wait Ended");
    return inventoryRepository.findBySkuCodeIn(skuCode).stream()
        .map(inventory ->
            InventoryResponse.builder()
                .skuCode(inventory.getSkuCode())
                .isInStock(inventory.getQuantity() > 0)
                .build()
        ).toList();
  }
}
